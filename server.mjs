import express from "express";
import { connect } from "mongoose";
import { exit } from "process";

import tasksRouter from "./routes/tasksRouter.mjs";

import authRouter from "./routes/authRouter.mjs";

import passport from "./lib/auth.mjs";

// First, connect to mongoose. If not connected - do not start application.
try {
	await connect("mongodb://127.0.0.1:37017/todoApplication");
	console.log("MongoDB connected");
} catch {
	console.log("Couldn't connect to MongoDB, terminating the application");
	exit(20);
}

const app = express();
const port = 3001;

// Set body-parser
app.use(express.json());

// Set up passport
app.use(passport.initialize());

// Set up auth router
app.use("/api/auth", authRouter(passport));

// Authenticate users
app.use(passport.authenticate("jwt", { session: false }));

// Set up routers
app.use(tasksRouter());

// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {
	const status = err.status ?? 500;

	if (err.status === 500) {
		console.error(err);
	} else {
		console.error(err.message);
	}

	res.status(status).send({ message: err.message });
});

app.listen(port, () => console.log(`API listening on port ${port}`));

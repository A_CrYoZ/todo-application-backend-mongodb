import { Router } from "express";
import createError from "http-errors";
import { check, validationResult, param } from "express-validator";

import TasksService from "../services/tasksService.mjs";

const router = new Router();

export default () => {
	// Set routes. I didn't create separate routers as there are only 5 routes
	router.get("/api/tasks/done", async (req, res, next) => {
		try {
			const { id } = req.user;

			const resTasks = await TasksService.getCompletedTasks(id);

			if (resTasks) {
				res.json(resTasks);
			} else {
				next(createError(404, "Tasks not found"));
			}
		} catch (error) {
			next(error);
		}
	});

	router.get("/api/tasks", async (req, res, next) => {
		try {
			const { id } = req.user;

			const resTasks = await TasksService.getIncompleteTasks(id);

			if (resTasks) {
				res.json(resTasks);
			} else {
				next(createError(404, "Tasks not found"));
			}
		} catch (error) {
			next(createError(500, "Unable to read incomplete tasks"));
		}
	});

	router.post(
		"/api/tasks",
		[
			check("title")
				.trim()
				.isLength({ min: 3 })
				.escape()
				.withMessage("Minimal title length for task is 3 letter!"),
			check("description")
				.trim()
				.isLength({ min: 3 })
				.escape()
				.withMessage("Minimal description length for task is 3 letter!"),
		],
		async (req, res, next) => {
			try {
				const validationErrors = validationResult(req);

				if (validationErrors.isEmpty()) {
					const { title, description } = req.body;

					const { id } = req.user;

					// Add task for user and if not exist - it will be automatically created
					const result = await TasksService.addTask(
						title,
						description,
						id,
						false
					);

					if (result) {
						// Get incomplete tasks for user to return to client
						const resTasks = await TasksService.getIncompleteTasks(id);
						res.status(201).json({ tasks: resTasks });
					} else {
						next(createError(400, `Task ${title} already exists!`));
					}
				} else {
					const errorMsg = validationErrors.array()[0].msg;
					next(createError(400, errorMsg));
				}
			} catch (error) {
				next(createError(500, "Unable to add task"));
			}
		}
	);

	router.post(
		"/api/tasks/done",
		check("title").isLength({ min: 3 }).escape(),
		async (req, res, next) => {
			try {
				const validationErrors = validationResult(req);

				if (validationErrors.isEmpty()) {
					const { title } = req.body;

					const { id } = req.user;

					const result = await TasksService.completeTaskByTitle(title, id);

					if (result) {
						// Get incomplete tasks for user to return to client
						const resTasks = await TasksService.getIncompleteTasks(id);

						res.json({ message: "ok", tasks: resTasks });
					} else {
						next(createError(404, "Couldn't find such task"));
					}
				} else {
					const errorMsg = validationErrors.array()[0].msg;
					next(createError(400, errorMsg));
				}
			} catch (error) {
				next(createError(500, "Unable to complete task"));
			}
		}
	);

	router.put(
		"/api/tasks/:taskTitle",
		[
			param("taskTitle")
				.isLength({ min: 3 })
				.escape()
				.withMessage("taskTitle cannot be less than 3 symbols"),
			check("title")
				.isLength({ min: 3 })
				.escape()
				.withMessage("title cannot be less than 3 symbols"),
			check("description")
				.isLength({ min: 3 })
				.escape()
				.withMessage("description cannot be less than 3 symbols"),
		],
		async (req, res, next) => {
			try {
				const validationErrors = validationResult(req);

				if (validationErrors.isEmpty()) {
					const { taskTitle } = req.params;
					const { title: newTitle, description: newDescription } = req.body;

					const { id } = req.user;

					const result = await TasksService.updateTask(
						taskTitle,
						newTitle,
						newDescription,
						id
					);

					if (result) {
						res.json({ message: "ok" });
					} else {
						next(createError(404, "Couldn't find such task"));
					}
				} else {
					let message = "";

					message = validationErrors
						.array()
						.reduce(
							(accumulator, currentValue) =>
								`${accumulator + currentValue.msg}. `,
							message
						);

					next(createError(400, message));
				}
			} catch (error) {
				if (error.message === "Duplicate") {
					next(createError(400, "Task with the same name is already exists!"));
				} else {
					next(createError(500, "Unable to update task"));
				}
			}
		}
	);

	router.delete(
		"/api/tasks",
		check("title")
			.isLength({ min: 3 })
			.escape()
			.withMessage("title cannot be less than 3 symbols"),
		async (req, res, next) => {
			try {
				const validationErrors = validationResult(req);

				if (validationErrors.isEmpty()) {
					const { title } = req.body;

					const { id } = req.user;

					const result = await TasksService.deleteTaskByTitle(title, id);

					if (result) {
						// Get incomplete tasks for user to return to client
						const resTasks = await TasksService.getIncompleteTasks(id);

						res.json({ message: "ok", tasks: resTasks });
					} else {
						next(createError(404, "Couldn't find such task"));
					}
				} else {
					const errorMsg = validationErrors.array()[0].msg;
					next(createError(400, errorMsg));
				}
			} catch (error) {
				next(createError(500, "Unable to delete task"));
			}
		}
	);

	return router;
};

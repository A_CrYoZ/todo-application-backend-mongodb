/* eslint-disable no-unused-vars */
import { Router } from "express";
import createError from "http-errors";
import { check, validationResult } from "express-validator";
import jwt from "jsonwebtoken";

import SECRET_KEY from "../configs/secret.mjs";

const router = new Router();

function checkValidationErrors(req, _, next) {
	const validationErrors = validationResult(req);

	if (validationErrors.isEmpty()) return next();

	let message = "";

	message = validationErrors
		.array()
		.reduce(
			(accumulator, currentValue) => `${accumulator + currentValue.msg}. `,
			message
		);

	return next(createError(400, message));
}

const credentialsValidators = [
	check("email")
		.trim()
		.isEmail()
		.isLength({ min: 6 })
		.withMessage("Email length cannot be less than 6 symbols"),
	check("password")
		.isStrongPassword({ minLength: 8 })
		.withMessage("Weak password"),
];

export default (passport) => {
	router.post(
		"/register",
		credentialsValidators,
		checkValidationErrors,
		passport.authenticate("register", { session: false }),
		(req, res, next) => {
			res.json({
				message: "Signup successful",
				user: req.user,
			});
		}
	);

	router.post("/login", async (req, res, next) => {
		// eslint-disable-next-line consistent-return
		passport.authenticate("login", async (err, user, info) => {
			try {
				if (err || !user) {
					return next(createError(400, "Invalid username or password!"));
				}

				req.login(user, { session: false }, async (error) => {
					if (error) return next(error);

					const body = {
						id: user.getId,
						iat: Date.now(),
					};
					const token = jwt.sign({ user: body }, SECRET_KEY, {
						expiresIn: "1d",
					});

					return res.json({ token });
				});
			} catch (error) {
				return next(error);
			}
		})(req, res, next);
	});

	return router;
};

import bcrypt from "bcrypt";

import DBService from "../modules/users/mongo/service/mongoUserService.mjs";
import User from "../modules/users/user.mjs";

class UserService {
	static async createUser(email, password) {
		const find = await DBService.findUser(email);

		if (find) throw new Error("The user already exist!");

		const hashedPassword = await bcrypt.hash(password, 10);

		const user = new User(email, hashedPassword);
		await DBService.createUser(user);

		return user;
	}

	static async isValidPassword(user, password) {
		const compare = await bcrypt.compare(password, user.getPassword);

		return compare;
	}

	static async findUser(email) {
		return DBService.findUser(email);
	}
}

export default UserService;

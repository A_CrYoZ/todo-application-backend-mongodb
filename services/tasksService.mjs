import Task from "../modules/tasks/task.mjs";
import DBService from "../modules/tasks/mongo/service/mongoTaskService.mjs";

function formatTasks(tasksFromDb) {
	const tasks = [];

	tasksFromDb.forEach((task) => {
		// eslint-disable-next-line no-shadow
		const { title, description, creator, done } = task;
		const newTask = new Task(title, description, creator, done);
		newTask.setId = task.id;

		tasks.push(newTask);
	});

	return tasks;
}

class Tasks {
	static async addTask(taskTitle, taskDescription, creator, taskCompleted) {
		return DBService.addTask(
			new Task(taskTitle, taskDescription, creator, taskCompleted)
		);
	}

	static async completeTaskByTitle(taskTitle, creator) {
		return DBService.completeTaskByTitle(taskTitle, creator);
	}

	static async getAllTasks(creator) {
		const results = await DBService.getAllTasks(creator);

		return formatTasks(results);
	}

	static async getCompletedTasks(creator) {
		const results = await DBService.getCompletedTasks(creator);

		return formatTasks(results);
	}

	static async getIncompleteTasks(creator) {
		const results = await DBService.getIncompleteTasks(creator);

		return formatTasks(results);
	}

	static async updateTask(oldName, newTitle, newDescription, creator) {
		return DBService.updateTask(oldName, newTitle, newDescription, creator);
	}

	static async deleteTaskByTitle(title, creator) {
		return DBService.deleteTaskByTitle(title, creator);
	}
}

export default Tasks;

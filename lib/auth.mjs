import passport from "passport";
import { Strategy as LocalStrategy } from "passport-local";
import { Strategy as JWTStrategy, ExtractJwt } from "passport-jwt";
import createError from "http-errors";

import SECRET_KEY from "../configs/secret.mjs";

import UserService from "../services/userService.mjs";

passport.use(
	"register",
	new LocalStrategy(
		{ usernameField: "email", passwordField: "password" },
		async (email, password, done) => {
			try {
				const user = await UserService.createUser(email, password);

				return done(null, { email: user.getEmail });
			} catch (error) {
				if (error.message === "The user already exist!")
					return done(createError(400, error.message));
				return done(error);
			}
		}
	)
);

passport.use(
	"login",
	new LocalStrategy(
		{ usernameField: "email", passwordField: "password" },
		async (email, password, done) => {
			try {
				const user = await UserService.findUser(email);

				if (!user) return done(null, false, "User not found");

				const validate = await UserService.isValidPassword(user, password);

				if (!validate) return done(null, false, "Wrong password");

				return done(null, user, { message: "Logged in Successfully" });
			} catch (error) {
				return done(error);
			}
		}
	)
);

passport.use(
	new JWTStrategy(
		{
			secretOrKey: SECRET_KEY,
			jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
		},
		async (token, done) => {
			try {
				return done(null, token.user);
			} catch (err) {
				return done(err);
			}
		}
	)
);

export default passport;

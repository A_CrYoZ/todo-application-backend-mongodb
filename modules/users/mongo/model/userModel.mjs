import { Schema, model } from "mongoose";

const userSchema = new Schema(
	{
		email: {
			type: String,
			required: true,
		},
		password: {
			type: String,
			required: true,
		},
	},
	{
		timestamps: true,
	}
);

userSchema.index({ email: 1 }, { unique: true });

export default model("Users", userSchema);

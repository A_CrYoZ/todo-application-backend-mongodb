import UserModel from "../model/userModel.mjs";
import User from "../../user.mjs";

class MongoUserService {
	static async createUser(userObj) {
		await UserModel.create({
			email: userObj.getEmail,
			password: userObj.getPassword,
		});
	}

	static async findUser(email) {
		const user = await UserModel.findOne({ email });

		let userObj = null;
		if (user) {
			userObj = new User(user.email, user.password);
			userObj.setId = user.id;
		}

		return userObj;
	}
}

export default MongoUserService;

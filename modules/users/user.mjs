class User {
	#id;

	#email;

	#password;

	constructor(email, password) {
		this.#id = null;
		this.#email = email;
		this.#password = password;
	}

	get getPassword() {
		return this.#password;
	}

	get getEmail() {
		return this.#email;
	}

	get getId() {
		return this.#id;
	}

	set setId(id) {
		this.#id = id;
	}
}

export default User;

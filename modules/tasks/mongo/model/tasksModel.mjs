import { Schema, model } from "mongoose";

const taskSchema = new Schema(
	{
		title: String,
		description: String,
		done: Boolean,
		creator: {
			type: Schema.Types.ObjectId,
			ref: "Users",
		},
	},
	{
		timestamps: true,
	}
);

taskSchema.index({ title: 1, creator: 1 }, { unique: true });

export default model("Tasks", taskSchema);

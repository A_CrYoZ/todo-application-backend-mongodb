class Task {
	#id;

	constructor(title, description, creator, done) {
		this.#id = null;
		this.title = title;
		this.description = description;
		this.done = done;
		this.creator = creator;
	}

	get getDescription() {
		return this.done;
	}

	set setDescription(description) {
		this.done = description;
	}

	get getCompleted() {
		return this.done;
	}

	set setCompleted(done) {
		this.done = done;
	}

	get getId() {
		return this.#id;
	}

	set setId(id) {
		this.#id = id;
	}
}

export default Task;
